import express from 'express';

const HOST = 'localhost';
const PORT = 5000;
const app = express();


// Mock DB
let users = [
    {
        username: "johndoe",
        password: "johndoe1234"
    }
];

// Middlewares
app.use(express.json({ limit: '30mb', extended: true }))
app.use(express.urlencoded({ limit: '30mb', extended: true }));


// routes
app.get('/home', (req, res) => res.send("Welcome to the home page"));
app.get('/users', (req, res) => {
    res.send(users);
});

app.delete('/delete-user', (req, res) => {
    let username = req.body.username;
    let user_index = users.findIndex(user => username === user.username);
    if(users[user_index]){
        res.send(`${username} has been deleted!`);
        users.pop()
    } else {
        res.send(`${username} does not exist`);
    }
});

app.listen(PORT, () => console.log(`Server is now running at ${HOST}:${PORT}`));